import 'dart:ffi';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:prototipo_iyuca/AnalyticsService.dart';
import 'package:prototipo_iyuca/model/product.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:prototipo_iyuca/services/authentication.dart';
import 'package:prototipo_iyuca/model/products_repository.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';


class hacerPedido extends StatefulWidget{
  hacerPedido({Key key, this.auth, this.userId, this.logoutCallback})
      : super(key: key);

  final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;



  @override
  State<StatefulWidget> createState() => _ProductListTab();

}


class _ProductListTab extends State<hacerPedido> {
  final _textController = TextEditingController();
  final Connectivity _connectivity = Connectivity();
  final databaseReference = Firestore.instance;
  final AnalyticsService _analyticsService = AnalyticsService();
  String _producto;
  String _medida;
  Category _cat;
  String _errorMessage;

  BluetoothState _bluetoothState = BluetoothState.UNKNOWN;

  @override
  void initState(){
    _producto = "Uvas";
    _medida = "KG";
    _errorMessage = "";
    super.initState();
  }

  void verificarEstadoBluetooth(){

    // Get current state
    FlutterBluetoothSerial.instance.state.then((state) {
      setState(() {
        _bluetoothState = state;
      });
    });

    //Escuchar cambios en el estado
    FlutterBluetoothSerial.instance
        .onStateChanged()
        .listen((BluetoothState state) {
      setState(() {
        _bluetoothState = state;
      });
    });
}

  @override
  void dispose(){
    _textController.dispose();
    _errorMessage = "";
    super.dispose();
  }

  guardarAnalytics(String user, String producto, int cantidad, String medida) async {
    _analyticsService.logPostCreated(user, producto, cantidad, medida);
    _analyticsService.logProducto(producto);
  }

  _guardarValores(context) async {
    if((_cat == Category.solidos && _medida =="Litros") || (_cat == Category.liquidos && (_medida =="Unidades" && _medida == "Gramos"))){
      setState(() {
        _errorMessage = "No coincide el producto y la medida";
      });
    }
    else {
      // obtain shared preferences
      final prefs = await SharedPreferences.getInstance();
      int c = int.parse(_textController.text);
      String user = await prefs.get('usuario');
      await prefs.setString('producto', _producto);
      await prefs.setInt('cantidad', c);
      await prefs.setString('medida', _medida);

      ConnectivityResult res;
      try {
        res = await _connectivity.checkConnectivity();
        res = await _connectivity.checkConnectivity();
        print("resultado" + res.toString());
        print(res != ConnectivityResult.wifi);
        print(res != ConnectivityResult.mobile);
        print(
            res == ConnectivityResult.wifi || res == ConnectivityResult.mobile);
        if (res == ConnectivityResult.mobile ||
            res == ConnectivityResult.wifi) {
          await guardarFirebase(user, _producto, c, _medida);

          Navigator.pushNamed(context, 'buscando2');
          guardarAnalytics(user, _producto, c, _medida);
        }
        else {
          Navigator.pushNamed(context, 'email');
        }
      }
      catch (ex) {

      }
    }
    /*
    verificarEstadoBluetooth();
    if (_bluetoothState.isEnabled){
      print("Esta activadoooo");
      Navigator.pushNamed(context, 'buscando');
    }
    else{
      print("No está activado");
      print(_bluetoothState.toString());
      _alerta();
    }*/
  }

  guardarFirebase(String user, String producto, int cantidad, String medida ) async {
    ConnectivityResult res;
    try {
      res = await _connectivity.checkConnectivity();
      if (res == ConnectivityResult.mobile || res == ConnectivityResult.wifi) {
        var op;
        var a = await databaseReference
            .collection("usuarios")
            .getDocuments()
            .then((QuerySnapshot snapshot) {

          snapshot.documents.forEach((f) {
            print(f['ID']);
            if(f['ID'] == user){
              op = f.reference;
              print("OP "+ op);
              f.documentID;
            }
          } );
        }).whenComplete(() {
          databaseReference.runTransaction((transaction)async{
            databaseReference.collection("prestamos")
                .add({
              'cantidad': cantidad,
              'medida': medida,
              'presta': null,
              'producto': producto,
              'recibe': op,
            });
          });
        });

      }
    }
    catch(ex){

    }

  }

  Future<void> _alerta() async{
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Bluetooth'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('El bluetooth debe estar encendido',
                    style: TextStyle(fontSize: 20) ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Aceptar',
              style: TextStyle(fontSize: 20, color: new Color.fromRGBO(153, 190, 30, 1.0),),),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget showErrorMessage() {
    if (_errorMessage.length > 0 && _errorMessage != null) {
      return new Text(
        _errorMessage,
        textScaleFactor: 1.0,
        style: TextStyle(
            fontSize: 16.0,
            color: Colors.red,
            height: 1.0,
          ),
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }
  @override
  Widget build(BuildContext context) {

    var lista = ProductsRepository.loadProducts(Category.all);
    var medidas = ['KG', 'Unidades', 'Litros', 'Gramos'];

    return Scaffold(
          appBar: AppBar(
            backgroundColor: new Color.fromRGBO(153, 190, 30, 1.0),
            title: Text("Hacer Pedido",
                textScaleFactor: 1.0,
                style: TextStyle(fontSize: 25)),
          ),

        body: ListView(

          padding: const EdgeInsets.all(8),
          children: <Widget>[
            Container(
              height: 20,
              margin: EdgeInsets.fromLTRB(0,120, 0, 0),
              child: const Center(child: Text('Escoja el producto a pedir',
                textScaleFactor: 1.0,
                style: TextStyle(fontSize: 20,color: Colors.black ), )),
            ),
            Container(
                height: 100,
                child: CupertinoPicker(itemExtent: 50,
                    scrollController: FixedExtentScrollController(initialItem: 0),
                    backgroundColor: Colors.white,
                    onSelectedItemChanged: (i) => setState(() {_producto = lista[i].name; _cat = lista[i].category;}),
                    children: lista != null ? lista.map((prod) {

                        return new Text(
                          prod.name,
                          textAlign: TextAlign.center,
                          textScaleFactor: 1.0,
                          style: new TextStyle(
                            fontSize: 20.0,

                        ),
                      );
                    }
                    ).toList():[]

                )
            ),
            Container(
              height: 20,
              margin: EdgeInsets.fromLTRB(0,30, 0, 0),
              child: const Center(child: Text('Cantidad',
                textScaleFactor: 1.0,
                style: TextStyle(fontSize: 20, color: Colors.black),)),
            ),
            Container(
              height: 50,
              margin: EdgeInsets.fromLTRB(0,10, 0, 0),
              child: Center(
                  child: CupertinoTextField(
                    decoration: BoxDecoration(color: Colors.white, border: Border.all(color: Colors.grey,width: 0.5), borderRadius: BorderRadius.all(Radius.circular(5.0))),
                    maxLength: 5,
                    controller: _textController,
                    keyboardType: TextInputType.numberWithOptions(signed: false, decimal: false) ,
                  ),
              ),
            ),
            Container(
              height: 20,
              margin: EdgeInsets.fromLTRB(0,50, 0, 0),
              child: const Center(child: Text('Unidad',
                textScaleFactor: 1.0,
                style: TextStyle(fontSize: 20, color: Colors.black),)),
            ),
            Container(
                height: 100,
                child: CupertinoPicker(itemExtent: 50,
                    scrollController: FixedExtentScrollController(initialItem: 0),
                    backgroundColor: Colors.white,
                    onSelectedItemChanged: (i) => setState(() => _medida = medidas[i]),
                    children: medidas != null ? medidas.map((med) {

                        return new Text(
                          med,
                          textAlign: TextAlign.center,
                          textScaleFactor: 1.0,
                          style: new TextStyle(
                            fontSize: 20.0,
                          ),

                      );
                    }
                    ).toList():[]
                )
            ),
            showErrorMessage(),
            Container(
              margin: EdgeInsets.fromLTRB(0,50, 0, 0),
              child: CupertinoButton(
                minSize: 50,
                child: Text("Pedir",
                    textScaleFactor: 1.0,
                    style: TextStyle(fontSize: 20, )),
                onPressed: () => _guardarValores(context),
                color: new Color.fromRGBO(153, 190, 30, 1.0),
              ),
            )
          ],
        )
    );


  }


}