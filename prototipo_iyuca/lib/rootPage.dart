import 'package:flutter/material.dart';
import 'package:prototipo_iyuca/AnalyticsService.dart';
import 'package:prototipo_iyuca/services/authentication.dart';
import 'package:prototipo_iyuca/LoginSingupPage.dart';
import 'package:prototipo_iyuca/inicio.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum AuthStatus {
  NOT_DETERMINED,
  NOT_LOGGED_IN,
  LOGGED_IN,
}

class RootPage extends StatefulWidget {
  RootPage({this.auth});

  final BaseAuth auth;

  @override
  State<StatefulWidget> createState() => new _RootPageState();
}

class _RootPageState extends State<RootPage>{

  final databaseReference = Firestore.instance;

  final AnalyticsService _analyticsService = AnalyticsService();

  AuthStatus authStatus = AuthStatus.NOT_DETERMINED;
  String _userId = "";
  String _mail = "";
  String _userName = "";
  int _local =-1;

  @override
  void initState() {
    super.initState();
    widget.auth.getCurrentUser().then((user) {
      setState(() {
        if (user != null) {
          _userId = user?.uid;
          _mail = user?.email;
        }
        authStatus =
        user?.uid == null ? AuthStatus.NOT_LOGGED_IN : AuthStatus.LOGGED_IN;
      });
    });

    if(_userId != "" && _userId != null){
      _userInDataBase(_userId,_mail,_userName,_local);
    }

  }

  _userInDataBase(user,mail,userN,local) async {

    CollectionReference usuarios = databaseReference.collection("usuarios");
    var esta = false;
    DocumentReference ref;
    var doc;
    var a = await databaseReference
        .collection("usuarios")
        .getDocuments()
        .then((QuerySnapshot snapshot) {
      snapshot.documents.forEach((f) {
        if(f['ID'] == user){
          esta = true;
          doc = f.documentID;
          ref = f.reference;
          true;
        }
      } );
    }).whenComplete((){
      if(!esta){
        databaseReference.runTransaction((transaction) async{
          usuarios.add(<String, dynamic>{
            'ID': user,
            'email':mail,
            'nombre':userN,
            'local': local,
          });
        });
      }
    });

    final prefs = await SharedPreferences.getInstance();

    await prefs.setString('usuario', user);
    await prefs.setString('documentID', doc);
    await prefs.setString('nombre', userN);
    await prefs.setString('ref', ref.path.split("/")[1]);

  }

  Widget buildWaitingScreen() {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: CircularProgressIndicator(),
      ),
    );
  }



  void loginCallback() {
    widget.auth.getCurrentUser().then((user) {
      setState(() {
        _userId = user.uid.toString();
        _mail = user.email;
        _userName = user.displayName;
        _local = int.parse(user.photoUrl);
      });
    });
    setState(() {
      authStatus = AuthStatus.LOGGED_IN;
    });
  }

  void logoutCallback() {
    setState(() {
      authStatus = AuthStatus.NOT_LOGGED_IN;
      _userId = "";
      _mail = "";
      _userName = "";
      _local = -1;
    });
  }

  @override
  Widget build(BuildContext context) {
    switch (authStatus) {
      case AuthStatus.NOT_DETERMINED:
        return buildWaitingScreen();
        break;
      case AuthStatus.NOT_LOGGED_IN:
        return new LoginSingupPage(
          auth: widget.auth,
          loginCallback: loginCallback,
        );
        break;
      case AuthStatus.LOGGED_IN:
        if (_userId.length > 0 && _userId != null) {
          _userInDataBase(_userId, _mail,_userName,_local);
          _analyticsService.logLogin();
          return new inicio(
            userId: _userId,
            auth: widget.auth,
            mail: _mail,
            logoutCallback: logoutCallback,
          );
        } else
          return buildWaitingScreen();
        break;
      default:
        return buildWaitingScreen();
    }
  }


}