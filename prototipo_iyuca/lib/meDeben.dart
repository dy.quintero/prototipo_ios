import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:prototipo_iyuca/CustomCacheManager.dart';
import 'package:prototipo_iyuca/StorageManager.dart';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'package:connectivity/connectivity.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MeDeben extends StatefulWidget {

  final StorageManager storage = StorageManager();

  @override
  State<StatefulWidget> createState() => new Deben();
}

class Deben extends  State<MeDeben> {

  List<String> meDeben;

  final Connectivity _connectivity = Connectivity();
  final databaseReference = Firestore.instance;
  CustomCacheManager cacheManager = CustomCacheManager();
  String documentId;

  String docRef;

  @override
  void initState(){
    setState(() {
      meDeben = [];
    });
    super.initState();
    getDocumentID();
    cache();
    initConnectivity();
  }

  getPedidos(user,id) async {
    List pedidos = [];

    await databaseReference
        .collection("prestamos")
        .getDocuments()
        .then((QuerySnapshot snapshot) {
      snapshot.documents.forEach((f) {
        if(f['presta']!= null && f['presta'].documentID.toString().trim() == id){
          pedidos.add(f.data);
        } });
    });

    print(pedidos);
    return pedidos;
  }


  Future<void> initConnectivity() async {
    ConnectivityResult result;
    final prefs = await SharedPreferences.getInstance();

    var user = prefs.get("usuario");
    var id = prefs.get("documentID");

    try {
      result = await _connectivity.checkConnectivity();
      if (result == ConnectivityResult.mobile || result == ConnectivityResult.wifi) {
        List<String> nuevo = [];
        List lista = await getPedidos(user, id);
        if(lista.isNotEmpty){
           lista.forEach((p)  =>  append(nuevo,p));
        }

        List remove = [];
        nuevo.forEach((l) {
          if (meDeben.contains(l)){
            remove.add(l);
          }
        });
        nuevo.removeWhere( (e) => remove.contains(e));

        setState(() {
          meDeben.addAll(nuevo);
        });

        guardarCache(nuevo);

      }
    } on PlatformException catch (e) {
      print(e.toString());
    }
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }
  }

  append(List lista, p){
    var s = p['recibe'].documentID + " " + p['cantidad'].toString() + " " + p['medida'] +" "+ p['producto'] ;
    lista.add(s);
  }

  append2(List lista, p){
    DocumentReference n = p['recibe'];
    n.get();
    var s = p['recibe'].documentID + " " + p['cantidad'].toString() + " " + p['medida'] +" "+ p['producto'] ;
    lista.add(s);
  }

  guardarCache(List lista) async {

    var a = await widget.storage.readLine2();

    var listaA = a.split(";");
    var str = '';
    lista.forEach((l){
      if(!listaA.contains(l)){
        str = str + l + ';';
      }
    }
    );

    await widget.storage.writeTransaction2(a+str);

  }

  cache() async{

    var a = await widget.storage.readLine2();

    var lista = a.split(";");
    List<String> values = [];
    lista.forEach((l) {
      if(!meDeben.contains(l)){
        values.add(l);
      }
    });

    setState(() {
      meDeben.addAll(values);
    });

  }

  Future<void> _alertaConexion() async{
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Error de conexión',
            textScaleFactor: 1.0,
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Detectamos que no hay conexión a internet. Por el momento no se pueden registrar pedidos como pagados',
                    textScaleFactor: 1.0,
                    style: TextStyle(fontSize: 20) ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Aceptar',
                textScaleFactor: 1.0,
                style: TextStyle(fontSize: 20, color: new Color.fromRGBO(255, 213, 77, 1.0),),),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget listaCache(BuildContext context){
    return  ListView.separated(
      separatorBuilder: (context, index) => Divider(
        color: Colors.blueGrey,
      ),
      padding: new EdgeInsets.fromLTRB(0.0,20.0,0.0,20.0),
      itemCount: meDeben.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          height: 50,
          child: Center(child: Text('${meDeben[index]}',
            textScaleFactor: 1.0,
          )),
        );
      },
    );
  }

  @override
  Widget build2(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 213, 77, 1.0),
        title: Text("Lo que me deben",
            textScaleFactor: 1.0,
            style: TextStyle(fontSize: 25)),
      ),

      body: Container(
          child: new ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Colors.blueGrey,
            ),
            padding: new EdgeInsets.fromLTRB(0.0,20.0,0.0,20.0),
            itemCount: meDeben.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                height: 50,
                child: Center(child: Text('${meDeben[index]}',
                  textScaleFactor: 1.0,
                )),
              );
            },
          )
      ),
    );
  }


  getDocumentID() async {
    final prefs = await SharedPreferences.getInstance();
    var id = prefs.get("documentID");
    var ref = prefs.get("ref");
    print("Referencia al documento:");
    print(ref);
    setState(() {
      documentId = id;
      docRef = ref;
    });
  }

  darEstado(DocumentSnapshot doc){

    String str = "";
    if(doc['estado'] == 'pagado' || doc['estado'] == 'Pagado'){
      str = 'Ya se pago el favor';
    }
    else{
      str = 'No se ha pagado el favor';
    }
    return str;
  }



  Widget _buildListItem(BuildContext context, DocumentSnapshot document){
    DocumentReference ref  = document['recibe'];
    return FutureBuilder(
      future: ref.get(),
      builder: (context,snapshot){
        if(snapshot.connectionState == ConnectionState.done){
          if(snapshot.hasData && snapshot.data.exists) {
            return ListTile(
                subtitle: Text(
                    darEstado(document)
                ),
                title: Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        convertir(document, snapshot.data),
                        textScaleFactor: 1.0,
                      ),
                    ),
                    Container(
                        child: document['estado'] != null ? Container(height: 0.0,) :RaisedButton(
                          elevation: 5.0,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0)),
                          onPressed: () async {
                            ConnectivityResult result;
                            result = await _connectivity.checkConnectivity();
                            if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
                              if(document['calificacion'] != null){
                                return null;
                              }
                              else {
                                  _alerta();
                                  ref = Firestore.instance.document(
                                      'usuarios/' + docRef);
                                  Firestore.instance.runTransaction((
                                      transaction) async {
                                    DocumentSnapshot freshSnap =
                                    await transaction.get(document.reference);
                                    await transaction.update(
                                        freshSnap.reference, {
                                      'estado': 'Pagado',
                                    });
                                  });
                                }
                                //Navigator.pushNamed(context, 'gracias');
                                //guardarAnalytics(
                                //    document.reference.documentID, documentId);
                            }
                            else{
                              return _alertaConexion();
                            }

                          },
                          child: Text(
                            "Pagado",
                            textScaleFactor: 1.0,
                          ),
                          color: document['estado'] != null ? new Color.fromRGBO(150, 150, 150, 1.0) :new Color.fromRGBO(255, 213, 77, 1.0),
                        )
                    ),


                  ],

                )
            );
          }
          else {
            return new Container();
          }
        }
        else {
          return new Container();
        }
      },
    );
  }


  String convertir(DocumentSnapshot doc, DocumentSnapshot data) {


    String str = "";
    if(data.exists) {
      if (data['nombre'] != null) {
        str = data['nombre'] + " " + doc['cantidad'].toString() + " " +
            doc['medida'] + " " + doc['producto'];
      }
      else if (data['email'] != null) {
        str = data['email'] + " " + doc['cantidad'].toString() + " " +
            doc['medida'] + " " + doc['producto'];
      }
    }
    return str;
  }

  List filtrar(snapshot) {
    List lista = [];
    snapshot.data.documents.forEach((f) {
      if (documentId != null && f['presta']!= null && f['presta'].documentID.toString().trim() == documentId) {
        lista.add(f);
      }
    });
    print(lista.length);
    return lista;
  }

  Future<void> _alerta() async{
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Pago exitoso',
            textScaleFactor: 1.0,
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Gracias por registrar el pago que te hicieron!',
                    textScaleFactor: 1.0,
                    style: TextStyle(fontSize: 20) ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Aceptar',
                textScaleFactor: 1.0,
                style: TextStyle(fontSize: 20, color: new Color.fromRGBO(153, 190, 30, 1.0),),),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 213, 77, 1.0),
          title: Text("Lo que me deben",
            textScaleFactor: 1.0,
            style: TextStyle(fontSize: 25)),
      ),
      body: StreamBuilder(
          stream: Firestore.instance.collection('prestamos').snapshots(),
          builder: (context, snapshot) {
            switch(snapshot.connectionState) {
              case ConnectionState.waiting:
                return listaCache(context);
              case ConnectionState.none:
                return listaCache(context);
              case ConnectionState.active:
              case ConnectionState.done:
              {
                if (!snapshot.hasData) {
                  return ListView(

                    children: <Widget>[
                      Container(
                          margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
                          child: Image.asset('assets/images/noHay.gif')
                      ),

                      Container(
                        height: 50,
                        margin: EdgeInsets.fromLTRB(35, 50, 35, 0),
                        child: Center(
                            child: Text('No tienes prestamos!',
                                textAlign: TextAlign.center,
                                textScaleFactor: 1.0,
                                style: TextStyle(
                                  fontSize: 25,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ))),
                      ),
                    ],
                  );
                }
                var l = filtrar(snapshot);
                if (l.isEmpty) return ListView(
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
                        child: Image.asset('assets/images/noHay.gif')
                    ),

                    Container(
                      height: 50,
                      margin: EdgeInsets.fromLTRB(35, 50, 35, 0),
                      child: Center(
                          child: Text('No tienes prestamos!',
                              textAlign: TextAlign.center,
                              textScaleFactor: 1.0,
                              style: TextStyle(
                                fontSize: 25,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              ))),
                    ),
                  ],
                );
                return ListView.separated(
                  separatorBuilder: (context, index) =>
                      Divider(
                        color: Colors.blueGrey,
                      ),
                  itemCount: l.length,
                  itemBuilder: (context, index) =>
                      _buildListItem(context, l[index]),
                );
              }
              default: return new Container();
            }
          }
    ),
    );
  }

}