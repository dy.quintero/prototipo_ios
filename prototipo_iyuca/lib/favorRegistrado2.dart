import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class FavorRegistrado2 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: new Color.fromRGBO(252,254,252, 1.0),
        body: ListView(
            children: <Widget> [
              Container(
                  margin: EdgeInsets.fromLTRB(5,50, 0, 0),
                  child: Image.asset('assets/images/pinia.gif')
              ),
              Container(
                  margin: EdgeInsets.fromLTRB(0,50, 0, 0),
                  child: Center(
                      child: Text('Gracias por prestar tu producto',
                        textScaleFactor: 1.0,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 25,
                          color:Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                      )
                  )
              ),
              Container(
                  margin: EdgeInsets.fromLTRB(0,50, 0, 0),
                  child: Center(
                      child: Text('Podrás ver el favor registrado en tu lista "Lo que me deben"',
                        textScaleFactor: 1.0,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 25,
                          color: new Color.fromRGBO(107, 103, 103, 1.0), ),
                      )
                  )
              ),
              Container(
                  margin: EdgeInsets.fromLTRB(100, 50, 100, 0),
                  child: CupertinoButton(
                    minSize: 50,
                    child: Text("Volver",
                        textScaleFactor: 1.0,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: new Color.fromRGBO(252,254,252, 1.0),
                        )),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    color: new Color.fromRGBO(255, 213, 77, 1.0),
                  ),
                ),
            ]
        )
    );
  }
}