import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'dart:typed_data';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:prototipo_iyuca/confirmacion.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';

class Iniciar extends StatefulWidget {

  final List<BluetoothDevice> devicesList = new List<BluetoothDevice>();

  @override
  State<StatefulWidget> createState() => new Buscando();
}

class _Message {
  int whom;
  String text;

  _Message(this.whom, this.text);
}


class Buscando extends State<Iniciar> {

  StreamSubscription<BluetoothDiscoveryResult> _streamSubscription;
  List<BluetoothDevice> _devices = List<BluetoothDevice>();
  FlutterBluetoothSerial bluetooth = FlutterBluetoothSerial.instance;
  bool isDiscovering;
  bool _connected = false;
  BluetoothConnection connection;
  List<_Message> messages = List<_Message>();
  String _messageBuffer = '';
  bool isConnecting = true;

  bool get isConnected => connection != null && connection.isConnected;


  @override
  void initState() {
    super.initState();
    isDiscovering = true;
    findDevices();
    if (isDiscovering) {
      _startDiscovery();
    }
    startTime();
  }

  void findDevices() async{

    List<BluetoothDevice> devices = [];

    // To get the list of paired devices
    try {
      devices = await bluetooth.getBondedDevices();
    } on PlatformException {
      print("Error");
    }


    setState(() {
      _devices = devices;
    });
  }

  //Empieza a buscar
  void _startDiscovery() async{

    _streamSubscription =
        FlutterBluetoothSerial.instance.startDiscovery().listen((r) {
          setState(() {
            if (!_devices.contains(r.device)) {
              print("Se va a agregar este dispositivo a la lista: " +
                  r.device.address.toString());
              _devices.add(r.device);
              print("Tamaño de la lista: " + _devices.length.toString());
              //me conecto al dispositivo
              print("me voy a conectar a:" + r.device.address.toString());
              connectToDevice(r.device);
            }
          });
        });
    _streamSubscription.onDone(() {
      setState(() {
        isDiscovering = false;
      });
    });
  }

  void connectToDevice(BluetoothDevice device) async {
    bool bonded = false;
    try {
      if (device.isBonded) {
        print('Unbonding with ${device.address}...');
        bonded = await FlutterBluetoothSerial.instance
            .removeDeviceBondWithAddress(device.address);
        print('Unbonding with ${device.address} has succed');
      }
      else{
        print('Bonding with ${device.address}...');
        bonded = await FlutterBluetoothSerial.instance
            .bondDeviceAtAddress(device.address);
        print(
            'Bonding with ${device.address} has ${bonded
                ? 'succed'
                : 'failed'}.');
      }
    }
    catch (ex) {
      print("Error");
    }
    if(!bonded){
      print("llorelo papá");
      _startDiscovery();
    }
  }


    startTime() async {
      var duration = new Duration(seconds: 40);
      return new Timer(duration, route);
    }

    route() {
      Navigator.pushReplacement(context, MaterialPageRoute(
          builder: (context) => Confirmacion()
      )
      );
    }

    buscar() async {
      final prefs = await SharedPreferences.getInstance();

      // Try reading data from the counter key. If it doesn't exist, return 0.
      var counter = prefs.getString('producto') ?? 0;
      var user = prefs.getString('usuario') ?? 0;

      print(counter);
      print("Usuario: ");
      print(user);
    }

    cancelar(context) async {
      final prefs = await SharedPreferences.getInstance();

      prefs.remove('producto');
      prefs.remove('cantidad');
      prefs.remove('medida');
      Navigator.of(context).pop();
    }

    @override
    dispose() {
      widget.devicesList.clear();

      _streamSubscription?.cancel();
      super.dispose();
    }

    @override
    Widget build(BuildContext context) {
      return Scaffold(
          body: ListView(children: <Widget>[
            Container(
                margin: EdgeInsets.fromLTRB(0, 100, 0, 0),
                height: 400,
                child: Image.asset('assets/images/banano.gif')
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
              child: Center(
                  child: Text('Estamos buscando quien te preste',
                      style: TextStyle(fontSize: 25, color: Colors.grey,))),
            ),
            Container(
                margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
                child: CupertinoButton(
                  minSize: 50,
                  child: Text("Cancelar",
                      style: TextStyle(
                        fontSize: 20,
                      )),
                  onPressed: () {
                    buscar();
                    cancelar(context);
                  },
                  color: CupertinoColors.systemRed,
                ))
          ]));
    }
  }