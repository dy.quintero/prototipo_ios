import 'dart:io';
import 'package:flutter/material.dart';
import 'package:prototipo_iyuca/CustomCacheManager.dart';
import 'package:prototipo_iyuca/rootPage.dart';
import 'package:prototipo_iyuca/services/authentication.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


class inicio extends StatefulWidget{
  inicio({Key key, this.auth, this.userId,this.mail, this.logoutCallback})
      : super(key: key);

  final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;
  final String mail;

  @override
  State<StatefulWidget> createState() => PaginaInicial();

}

class PaginaInicial extends State<inicio> {

  //static const platform = MethodChannel('com.example.services_demo/service');

  CustomCacheManager cacheManager = CustomCacheManager();
  @override
  void initState() {

    //getData();
    guardarUser();
    super.initState();
    //connectToService();

  }

  /*Future<void> connectToService() async {
    try {
      await platform.invokeMethod<void>('connect');
      print('Connected to service');
    } on Exception catch (e) {
      print(e.toString());
    }
  }

  Future<String> getDataFromService() async {
    try {
      final result = await platform.invokeMethod<String>('start');
      return result;
    } on PlatformException catch (e) {
      print(e.toString());
    }
    return 'No Data From Service';
  }*/


  @override
  void dispose(){
    super.dispose();
  }

  final databaseReference = Firestore.instance;

  guardarUser() async {
    final prefs = await SharedPreferences.getInstance();

    await prefs.setString('usuario', widget.userId);

  }

  logOut(BuildContext context){
    widget.auth.signOut();
    widget.logoutCallback();
    new RootPage(auth: new Auth());
  }

  navegarA(BuildContext context,String ruta){
    Navigator.pushNamed(context, ruta);
  }

  void getData() {
    databaseReference
        .collection("usuarios")
        .getDocuments()
        .then((QuerySnapshot snapshot) {
      snapshot.documents.forEach((f) => print('${f.data}}'));
    });
  }


  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: AppBar(
        backgroundColor: new Color.fromRGBO(153, 190, 30, 1.0),
        title: Text("Menú principal",
            textScaleFactor: 1.0,
            style: TextStyle(fontSize: 25)),
      ),

      body: ListView(
          children: <Widget>[
             Container(
                  margin: EdgeInsets.fromLTRB(0,50,0, 0),
                  child: Center(
                    child: Text( "Bienvenido de nuevo",
                          textScaleFactor: 1.0,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 40,
                              fontWeight: FontWeight.bold
                          )
                      ),
                  )
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                child: Image.asset('assets/images/bienvenido.gif')
            ),
            Container(
              margin: EdgeInsets.fromLTRB(50,0,50, 0),
              child: new RaisedButton(
                  elevation: 5.0,
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0)),
                  color: new Color.fromRGBO(153, 190, 30, 1.0),
                  child: new Text("Solicitudes de favores",
                      textScaleFactor: 1.0,
                      textAlign: TextAlign.center,
                      style: new TextStyle(fontSize: 20.0, color: Colors.white)),
                  onPressed: () {
                    navegarA(context,'pendientes');
                  }
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(50,80,50, 0),
              child: new RaisedButton(
                  elevation: 5.0,
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0)),
                  color: new Color.fromRGBO(153, 190, 30, 1.0),
                  child: new Text("Hacer Pedido",
                      textScaleFactor: 1.0,
                      textAlign: TextAlign.center,
                      style: new TextStyle(fontSize: 20.0, color: Colors.white)),
                  onPressed: () => navegarA(context,'pedido')
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(50,10,50, 0),
              child: new RaisedButton(
                elevation: 5.0,
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20.0)),
                  color: new Color.fromRGBO(249, 101, 116, 1.0),
                  child: new Text("Lo que debo",
                      textAlign: TextAlign.center,
                      textScaleFactor: 1.0,
                      style: new TextStyle(fontSize: 20.0, color: Colors.white)),
                  onPressed: () => navegarA(context, 'debo')
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(50,10,50, 0),
              child: new RaisedButton(
                  elevation: 5.0,
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0)),
                  color: new Color.fromRGBO(255, 213, 77, 1.0),
                  child: new Text("Lo que me deben",
                      textAlign: TextAlign.center,
                      textScaleFactor: 1.0,
                      style: new TextStyle(fontSize: 20.0, color: Colors.white)),
                  onPressed: () => navegarA(context,'deben')
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(50,10,50, 0),
              child: new RaisedButton(
                  elevation: 5.0,
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0)),
                  color: new Color.fromRGBO(249, 101, 116, 1.0),
                  child: new Text("Cerrar sesión",
                      textAlign: TextAlign.center,
                      textScaleFactor: 1.0,
                      style: new TextStyle(fontSize: 20.0, color: Colors.white)),
                  onPressed: () => logOut(context)
              ),
            ),

          ]

      )
    );
  }
}
