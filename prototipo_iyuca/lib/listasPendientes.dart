import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:prototipo_iyuca/AnalyticsService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Pendientes extends StatefulWidget{

  @override
  State<StatefulWidget> createState() => _Pendientes();

}

class _Pendientes extends State<Pendientes>{

  final databaseReference = Firestore.instance;

  final Connectivity _connectivity = Connectivity();

  final AnalyticsService _analyticsService = AnalyticsService();

  List<String> pedidos;

  String documentId;

  String docRef;



  @override
  void initState(){

    super.initState();

    getDocumentID();

  }


  getDocumentID() async {
    final prefs = await SharedPreferences.getInstance();
    var id = prefs.get("documentID");
    var ref = prefs.get("ref");
    print("Referencia al documento:");
    print(ref);
    DocumentReference ref2;

    setState(() {
      documentId = id;
      docRef = ref;
    });
  }

  guardarAnalytics(String pedidoId,String userId) async {
    _analyticsService.logPedidoAceptado(pedidoId,userId);
  }

  Future<void> _alerta() async{
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Error de conexión',
            textScaleFactor: 1.0,
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Detectamos que no hay conexión a internet. Por el momento no se pueden aceptar solicitudes de pedidos',
                    textScaleFactor: 1.0,
                    style: TextStyle(fontSize: 20) ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Aceptar',
                textScaleFactor: 1.0,
                style: TextStyle(fontSize: 20, color: new Color.fromRGBO(153, 190, 30, 1.0),),),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }


  Widget _buildListItem(BuildContext context, DocumentSnapshot document){
      DocumentReference ref  = document['recibe'];
      return FutureBuilder(
        future: ref.get(),
        builder: (context,snapshot){
          if(snapshot.connectionState == ConnectionState.done){
            if(snapshot.hasData) {
              return ListTile(
                  title: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          convertir(document, snapshot.data),
                          textScaleFactor: 1.0,
                        ),
                      ),
                      Container(
                          child: RaisedButton(
                            elevation: 5.0,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(30.0)),
                            onPressed: () async {
                              ConnectivityResult result;
                              result = await _connectivity.checkConnectivity();
                              if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
                                ref = Firestore.instance.document('usuarios/' + docRef);
                                Firestore.instance.runTransaction((
                                    transaction) async {
                                  DocumentSnapshot freshSnap =
                                  await transaction.get(document.reference);
                                  await transaction.update(freshSnap.reference, {
                                    'presta': ref,
                                  });
                                });

                                Navigator.pushNamed(context, 'gracias');
                                guardarAnalytics(
                                    document.reference.documentID, documentId);
                              }
                              else{
                                _alerta();
                              }


                            },
                            child: Text("Yo puedo prestar",
                              textScaleFactor: 1.0,
                            ),
                            color: new Color.fromRGBO(255, 213, 77, 1.0),
                          )
                      ),


                    ],

                  )
              );
            }
            else {
              return new Container();
            }
          }
          else {
            return new Container();
          }
        },
      );
  }


  String convertir(DocumentSnapshot doc, data)  {

    DocumentReference ref = doc['recibe'];
    String paths = ref.path.split("/")[1] ;
    print(ref.path);
    String str = "";
    print(paths);
    if(data['nombre'] != null){
    str = doc['cantidad'].toString()+" "+doc['medida']+" "+doc['producto']+''' 
      Solicitante: ''' +data['nombre'];
    }
    else if(data['email'] != null){
      str = doc['cantidad'].toString()+" "+doc['medida']+" "+doc['producto']+''' 
      Solicitante: ''' +data['email'];
    }


    return str;
  }

  List filtrar(snapshot){
    List lista = [];
    snapshot.data.documents.forEach((f){
      if(documentId != null && f['recibe'].documentID.toString().trim() != documentId && f['presta'] == null  ){
        lista.add(f);
      }
    });
    print(lista.length);
    return lista;
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: new Color.fromRGBO(153, 190, 30, 1.0),
        title: Text("Pedidos",
            textScaleFactor: 1.0,
            style: TextStyle(fontSize: 25)),
      ),
      body: StreamBuilder(
        stream: Firestore.instance.collection('prestamos').snapshots(),
        builder: (context,snapshot){
          if(!snapshot.hasData){
            return ListView(

              children: <Widget>[
                Container(
                    margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
                    child: Image.asset('assets/images/noHay.gif')
                ),

                  Container(
                    height: 50,
                    margin: EdgeInsets.fromLTRB(35, 50, 35, 0),
                    child: Center(
                        child: Text('Aún no hay pedidos',
                          textAlign: TextAlign.center,
                            textScaleFactor: 1.0,
                            style: TextStyle(
                            fontSize: 25,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ))),
                ),
              ],
            );
          }
          var l = filtrar(snapshot);
          if (l.isEmpty) return ListView(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
                  child: Image.asset('assets/images/noHay.gif')
              ),

              Container(
                height: 50,
                margin: EdgeInsets.fromLTRB(35, 50, 35, 0),
                child: Center(
                    child: Text('Aún no hay pedidos',
                        textAlign: TextAlign.center,
                        textScaleFactor: 1.0,
                        style: TextStyle(
                          fontSize: 25,
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                        ))),
              ),
            ],
          );
          return ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Colors.blueGrey,
            ),
            itemCount: l.length,
            itemBuilder: (context, index)=>
                _buildListItem(context, l[index]),
          );
        }
      ),
    );
  }
}