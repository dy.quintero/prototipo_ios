import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/cupertino.dart';
import 'package:prototipo_iyuca/agitar.dart';
import 'package:prototipo_iyuca/buscando.dart';
import 'package:prototipo_iyuca/buscando2.dart';
import 'package:prototipo_iyuca/confirmacion.dart';
import 'package:prototipo_iyuca/debo2.dart';
import 'package:prototipo_iyuca/favorRegistrado2.dart';
import 'package:prototipo_iyuca/favorResgistrado.dart';
import 'package:prototipo_iyuca/listasPendientes.dart';
import 'package:prototipo_iyuca/porEmail.dart';
import 'package:prototipo_iyuca/services/authentication.dart';
import 'package:prototipo_iyuca/AnalyticsService.dart';
import 'package:flutter/material.dart';
import 'hacerPedido.dart';   // NEW
import 'debo.dart';         // NEW
import 'meDeben.dart';  // NEW
import 'rootPage.dart';


class CupertinoStoreApp extends StatelessWidget {

  final AnalyticsService _analyticsService = AnalyticsService();

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
      ),
      routes: {
        'pedido': (context) => hacerPedido(),
        'buscando': (context) => Iniciar(),
        'confirmacion': (context) => Confirmacion(),
        'agitar': (context) => Agitar(),
        'registrado': (context) => FavorRegistrado(),
        'debo' :(context) => Debo(),
        'deben': (context) => MeDeben(),
        'buscando2': (context) => BuscandoNuevo(),
        'pendientes': (context) => Pendientes(),
        'email':(context) => porEmail(),
        'gracias':(context) => FavorRegistrado2(),
      },
      home: new RootPage(auth: new Auth()),
      navigatorObservers:[
        _analyticsService.getObserver(),
      ],
    );

  }
}
