import 'dart:async';
import 'dart:io';


import 'package:path_provider/path_provider.dart';

class StorageManager {

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    print(directory.path);
    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/debo.txt');
  }

  Future<String> get _localPath2 async {
    final directory = await getApplicationDocumentsDirectory();
    print(directory.path);
    return directory.path;
  }

  Future<File> get _localFile2 async {
    final path = await _localPath2;
    return File('$path/deben.txt');
  }

  Future<File> writeTransaction(String valor) async {

    final file = await _localFile;

    // Write the file.
    return file.writeAsString(valor );
  }

  Future<String> readLine() async {
    try {
      final file = await _localFile;

      // Read the file.
      String contents = await file.readAsString();

      return contents;
    } catch (e) {
      // If encountering an error, return 0.
      return '';
    }
  }

  Future<File> writeTransaction2(String valor) async {

    final file = await _localFile2;

    // Write the file.
    return file.writeAsString(valor );
  }

  Future<String> readLine2() async {
    try {
      final file = await _localFile2;

      // Read the file.
      String contents = await file.readAsString();

      return contents;
    } catch (e) {
      // If encountering an error, return 0.
      return '';
    }
  }
}