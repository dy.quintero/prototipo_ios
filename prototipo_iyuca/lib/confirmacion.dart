import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:prototipo_iyuca/StorageManager.dart';
import 'package:prototipo_iyuca/agitar.dart';
import 'dart:async';
import 'package:prototipo_iyuca/CustomCacheManager.dart';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';





class Confirmacion extends StatefulWidget{

  final StorageManager storage = StorageManager();

  @override
  State<StatefulWidget> createState() => new Confirmacion2();
}

class Confirmacion2 extends State<Confirmacion> {

  CustomCacheManager cacheManager = CustomCacheManager();


  _terminar(context){
    Navigator.pushReplacement(context, MaterialPageRoute(
        builder: (context) => Agitar()
    )
    );
  }

  @override
  void initState() {
    super.initState();
    startTime();
    guardar();
  }

  startTime() async {
    var duration = new Duration(seconds: 10);
    return new Timer(duration, route);
  }

  guardar() async {
    final prefs = await SharedPreferences.getInstance();

    var usuario = await prefs.get("usuario");

    var producto = await prefs.getString('producto');

    var cantidad = await prefs.getInt('cantidad');

    var medida = await prefs.getString('medida');

    var tot = "usuario1" + " " + cantidad.toString()+ " " + medida + " "+ producto+";";
    var str = await widget.storage.readLine();
    widget.storage.writeTransaction(str+tot);

  }

  route() {
    Navigator.pop(context);
  }



  @override
  void dispose() {
    //_timer2.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    //_Act_NotificationScreenState(context);

    return Scaffold(
      body: ListView(
      children: <Widget>[
            Container(
                margin: EdgeInsets.fromLTRB(0,200, 0, 0),
                child: Center(
                  child: Text('John Jairo te va a prestar ',
                      style: TextStyle(fontSize: 25, color:Colors.black, ))),
                ),
        Container(
            child: Row(

            children: <Widget>[
              CupertinoButton(
                    child: Text("Aceptar",
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.white
                        )),
                    onPressed: ()=> _terminar(context),
                    color: Colors.green,
                  ),
              CupertinoButton(
                    child: Text("Cancelar",
                        style: TextStyle(
                          fontSize: 20,
                            color: Colors.white,
                        )),
                    onPressed: () {
                      //_timer2.cancel();
                      //Navigator.popUntil(context, ModalRoute.withName('buscando'));
                      Navigator.pop(context);
                    },
                    color: Colors.red,
                  ),
                ],
              ))
    ]));
  }
}
