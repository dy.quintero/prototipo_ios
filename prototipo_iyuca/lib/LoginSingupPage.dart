import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:prototipo_iyuca/AnalyticsService.dart';
import 'package:prototipo_iyuca/services/authentication.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';



class LoginSingupPage extends StatefulWidget{

  LoginSingupPage({this.auth, this.loginCallback});

  final BaseAuth auth;
  final VoidCallback loginCallback;

  @override
  State<StatefulWidget> createState() => new _LoginSingupPageState();

}

class _LoginSingupPageState extends State<LoginSingupPage>{

  final Connectivity _connectivity = Connectivity();

  final AnalyticsService _analyticsService = AnalyticsService();

  final PrimaryColor = const Color.fromARGB(255, 52, 199, 89);
  bool _isLoginForm;
  bool _isLoading;

  String _userName;
  String _email;
  String _password;
  String _local;
  String _errorMessage;

  final _formKey = new GlobalKey<FormState>();

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    _isLoginForm = true;
    super.initState();
  }

  Widget _showForm() {
    return new Container(
        padding: EdgeInsets.all(16.0),
        child: new Form(
          key: _formKey,
          child: new ListView(
            shrinkWrap: true,
            children: <Widget>[
              showUserNameInput(),
              showLocalInput(),
              showEmailInput(),
              showPasswordInput(),
              showPrimaryButton(),
              showSecondaryButton(),
              showErrorMessage(),
            ],
          ),
        ));
  }


  void toggleFormMode() {
    resetForm();
    setState(() {
      _isLoginForm = !_isLoginForm;
    });
  }


  Future<void> _alerta() async{
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Error de conexión',
            textScaleFactor: 1.0,
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Debe estar conectado a internet para proceder con el registro',
                    textScaleFactor: 1.0,
                    style: TextStyle(fontSize: 20) ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Aceptar',
                textScaleFactor: 1.0,
                style: TextStyle(fontSize: 20, color: new Color.fromRGBO(153, 190, 30, 1.0),),),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _mensajeIniciaSesion() async{
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Registro Exitoso',
            textScaleFactor: 1.0,
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('El registro fue exitoso, ahora puedes iniciar sesión',
                    textScaleFactor: 1.0,
                    style: TextStyle(fontSize: 20) ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Aceptar',
                textScaleFactor: 1.0,
                style: TextStyle(fontSize: 20, color: new Color.fromRGBO(153, 190, 30, 1.0),),),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }



  void resetForm() {
    _formKey.currentState.reset();
    _errorMessage = "";
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Login",
          textScaleFactor: 1.0,
        ),
        backgroundColor: new Color.fromRGBO(153, 190, 30, 1.0),
      ),
        body: Stack(
          children: <Widget>[
            _showForm(),
            //showCircularProgress(),
          ],
        )
    );
  }


  Widget showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  // Check if form is valid before perform login or signup
  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit() async {

      ConnectivityResult result;

      try {
        result = await _connectivity.checkConnectivity();
        if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {

          setState(() {
            _errorMessage = "";
            _isLoading = true;
          });
          if (validateAndSave()) {
            String userId = "";
            try {
              if (_isLoginForm) {
                userId = await widget.auth.signIn(_email, _password);
                _analyticsService.setUserProperties(userId: userId);
                var a = await widget.auth.getCurrentUser();
                a.email;
                print('Signed in: $userId');
              } else {
                userId = await widget.auth.signUp(_email, _password);
                //widget.auth.sendEmailVerification();
                //_showVerifyEmailSentDialog();
                print('Signed up user: $userId');
                var a = await widget.auth.getCurrentUser();
                var userUp = new UserUpdateInfo();
                userUp.displayName = _userName;
                userUp.photoUrl = _local.toString();
                await a.updateProfile(userUp);
                await a.reload();
                a = await widget.auth.getCurrentUser();
                _mensajeIniciaSesion();
                _analyticsService.logPrimerIngreso();
              }
              setState(() {
                _isLoading = false;
              });

              if (userId.length > 0 && userId != null && _isLoginForm) {
                _analyticsService.setUserProperties(userId: userId);
                widget.loginCallback();
              }
            } catch (e) {
              setState(() {
                _isLoading = false;
                _errorMessage = e.message;
                _formKey.currentState.reset();
              });
            }
        }
        }
        else{
          _alerta();
        }
      } on PlatformException catch (e) {
        print(e.toString());
      }

      // If the widget was removed from the tree while the asynchronous platform
      // message was in flight, we want to discard the reply rather than calling
      // setState to update our non-existent appearance.
      if (!mounted) {
        return Future.value(null);
      }


  }

  Widget showLogo() {
    return new Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.fromLTRB(0.0, 70.0, 0.0, 0.0),
        child: CircleAvatar(
          backgroundColor: Colors.transparent,
          radius: 48.0,
          child: Image.asset('assets/flutter-icon.png'),
        ),
      ),
    );
  }

  Widget showUserNameInput() {
    if(_isLoginForm){
      return new Container();
    }
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 0.0),
      child: new TextFormField(
        maxLength: 50,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'User name',
            icon: new Icon(
              Icons.perm_identity,
              color: Colors.grey,
            )),
          validator: (value) => value.isEmpty ? 'Usuario no puede estar vacio' : null,
          onSaved: (value) => _userName = value.trim(),
      ),
    );
  }

  Widget showLocalInput() {
    if(_isLoginForm){
      return new Container();
    }
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 0.0),
      child: new TextFormField(
        maxLength: 5,
        maxLines: 1,
        keyboardType: TextInputType.number,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Numero del Local',
            icon: new Icon(
              Icons.store_mall_directory,
              color: Colors.grey,
            )),
        validator: (value) => value.isEmpty ? 'El local no puede estar vacio' : null,
        onSaved: (value) => _local = value.trim(),
      ),
    );
  }


  Widget showEmailInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 0.0),
      child: new TextFormField(
        maxLength: 50,
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Email',
            icon: new Icon(
              Icons.mail,
              color: Colors.grey,
            )),
        validator: (value) => value.isEmpty ? 'Email can\'t be empty' : null,
        onSaved: (value) => _email = value.trim(),
      ),
    );
  }

  Widget showPasswordInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextFormField(
        maxLength: 20,
        maxLines: 1,
        obscureText: true,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Password',
            icon: new Icon(
              Icons.lock,
              color: Colors.grey,
            )),
        validator: (value) => value.isEmpty ? 'Password can\'t be empty' : null,
        onSaved: (value) => _password = value.trim(),
      ),
    );
  }

  Widget showErrorMessage() {
    if (_errorMessage.length > 0 && _errorMessage != null) {
      return new Text(
        _errorMessage,
        textScaleFactor: 1.0,
        style: TextStyle(
            fontSize: 13.0,
            color: Colors.red,
            height: 1.0,
            fontWeight: FontWeight.w300),
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }

  Widget showPrimaryButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 40.0,
          child: new RaisedButton(
            elevation: 5.0,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
            color: new Color.fromRGBO(153, 190, 30, 1.0),
            child: new Text(_isLoginForm ? 'Login' : 'Create account',
                textScaleFactor: 1.0,
                style: new TextStyle(fontSize: 20.0, color: Colors.white)),
            onPressed: validateAndSubmit,
          ),
        ));
  }

  Widget showSecondaryButton() {
    return new FlatButton(
        child: new Text(
            _isLoginForm ? 'Create an account' : 'Have an account? Sign in',
            textScaleFactor: 1.0,
            style: new TextStyle(fontSize: 18.0, fontWeight: FontWeight.w300)),
        onPressed: toggleFormMode);
  }


}
