import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BuscandoNuevo extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(children: <Widget>[
          Container(
              margin: EdgeInsets.fromLTRB(0, 100, 0, 0),
              height: 400,
              child: Image.asset('assets/images/banano.gif')
          ),
          Container(
            margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
            child: Center(
                child: Text('Se registro tu pedido con exito! Ya estamos buscando quien te pueda prestar',
                    textScaleFactor: 1.0,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 25, color: Colors.grey,))),
          ),
          Container(
              margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
              child: CupertinoButton(
                minSize: 50,
                child: Text("Volver",
                    textScaleFactor: 1.0,
                    style: TextStyle(
                      fontSize: 20,
                    )),
                onPressed: () {
                  Navigator.of(context).pop();
                },
                color: new Color.fromRGBO(153, 190, 30, 1.0),
              ))
        ]));
  }
}