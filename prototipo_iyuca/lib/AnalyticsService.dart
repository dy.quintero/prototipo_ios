import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/cupertino.dart';

class AnalyticsService{

  final FirebaseAnalytics _analytics = FirebaseAnalytics();

  FirebaseAnalyticsObserver getObserver() => FirebaseAnalyticsObserver(analytics: _analytics);

  Future setUserProperties({@required String userId}) async{
    await _analytics.setUserId(userId);
    print("Observer : Se logeo");
  }

  Future logLogin() async{
    await _analytics.logLogin(loginMethod: 'email');
  }

  Future logPostCreated(String usuario,String producto, int cantidad, String medida)async {
    await _analytics.logEvent(
      name: 'Pedidos',
      parameters: {
        'pide': usuario,
        'producto': producto,
        'cantidad':cantidad,
        'medida': medida,
      },
    );
  }

  Future logPrimerIngreso() async {
    await _analytics.logSignUp(signUpMethod: 'mail');
  }

  Future logAppOpen() async {
    await _analytics.logAppOpen();
  }

  Future logPedidoAceptado(String pedido, String mail)async {
    await _analytics.logEvent(
      name: 'Pedidosfinalizados',
      parameters: {
        'Pedido': pedido,
        'User': mail,
      },
    );
  }

  Future logProducto(String producto) async {
    await _analytics.logEvent(
        name: 'Producto',
    parameters: {
          'Producto': producto,
    });
    print("Observer : Producto");
  }

}