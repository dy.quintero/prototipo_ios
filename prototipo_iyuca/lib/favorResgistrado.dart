import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:prototipo_iyuca/debo.dart';
import 'package:prototipo_iyuca/inicio.dart';

class FavorRegistrado extends StatelessWidget {

  Timer _timer;

  _Act_NotificationScreenState(context) {
    _timer = new Timer(const Duration(seconds: 5), () {
      _timer = new Timer(const Duration(seconds: 1), () {
        _timer.cancel();
        return Navigator.pushReplacement(context, MaterialPageRoute(
            builder: (context) => SearchTab()
        )
        );
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    _Act_NotificationScreenState(context);
    return Scaffold(
        body: ListView(
            children: <Widget> [
              Container(
                  margin: EdgeInsets.fromLTRB(0,200,60, 0),
                  child: Image.asset('assets/images/sandia.gif')
              ),
              Container(
                  margin: EdgeInsets.fromLTRB(0,50,0,0),
                  child: Center(
                      child: Text('Se ha registrado tu favor con exito',
                        textScaleFactor: 1.0,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 25,
                              color:Colors.black, ),
                      )
                  )
              )
            ]
        )
    );

  }

}