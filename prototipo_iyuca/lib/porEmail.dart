import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:prototipo_iyuca/favorResgistrado.dart';
import 'package:shared_preferences/shared_preferences.dart';

class porEmail extends StatefulWidget{

  @override
  State<StatefulWidget> createState() => _porEmail();
}

class _porEmail extends State<porEmail>{

  final _textController = TextEditingController();

  @override
  void dispose(){
    _textController.dispose();

    super.dispose();
  }

  _save(context) async {

    final prefs = await SharedPreferences.getInstance();
    String p = _textController.text;
    await prefs.setString("email", p);
    print(p);
    Navigator.pushReplacement(context, MaterialPageRoute(
        builder: (context) => FavorRegistrado()
    ));

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: new Color.fromRGBO(255,243,236, 1.0),
      body: ListView(
        children: <Widget>[
          Container(
              margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Image.asset('assets/images/tomate.gif')
          ),
          Container(
            margin: EdgeInsets.fromLTRB(35, 20, 35, 0),
            child: Center(
                child: Text('Ops! Parece que no tienes internet, sin embargo aun puedes realizar tu pedido',
                    textScaleFactor: 1.0,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 25,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                    ))),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(20, 30, 20, 0),
              child: Center(
                  child: Text('Ingresa el correo de la persona que te va a prestar', textAlign: TextAlign.center,
                      textScaleFactor: 1.0,
                      style: TextStyle(
                        fontSize: 25,
                        color: new Color.fromRGBO(107, 103, 103, 1.0),
                      ))),
          ),
          Container(
            height: 100,
            margin: EdgeInsets.fromLTRB(10,60, 10, 0),
              child: Center(
                  child: CupertinoTextField(
                    decoration: BoxDecoration(color: Colors.white, border: Border.all(color: Colors.grey,width: 0.5), borderRadius: BorderRadius.all(Radius.circular(5.0))),
                    maxLength: 50,
                    controller: _textController,
                    keyboardType: TextInputType.emailAddress,
                  ),
              ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(100,60,100, 0),
            child: CupertinoButton(
              minSize: 50,
              child: Text("Pedir",
                  textScaleFactor: 1.0,
                  style: TextStyle(fontSize: 20, )),
              onPressed: () => _save(context),
              color: new Color.fromRGBO(249, 101, 116, 1.0),
            ),
          ),
        ]
      ),
    );
  }

}