import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:prototipo_iyuca/CustomCacheManager.dart';
import 'package:prototipo_iyuca/StorageManager.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'package:connectivity/connectivity.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


class Debo extends StatefulWidget{

  final StorageManager storage = StorageManager();

  @override
  State<StatefulWidget> createState() => _Debo();
}

class _Debo extends State<Debo> {

  TextEditingController _c;

  List<String> pedidos;

  final Connectivity _connectivity = Connectivity();
  final databaseReference = Firestore.instance;

  CustomCacheManager cacheManager = CustomCacheManager();

  String documentId;

  String docRef;

  int _cal;

  String _errorMessage;

  @override
  void initState() {

    setState(() {
      pedidos = [];
      _cal = -1;
      _errorMessage = "";
    });
    _c = new TextEditingController();

    super.initState();
    getDocumentID();
    cache();
    initConnectivity();

  }

  @override
  void dispose(){
    _c.dispose();
    _errorMessage = "";
    super.dispose();
  }

  getPedidos(user,id) async {

    List pedidos = [];
    await databaseReference
        .collection("prestamos")
        .getDocuments()
        .then((QuerySnapshot snapshot) {
      snapshot.documents.forEach((f) async  {
        if(f['recibe'].documentID.toString().trim() == id && f['presta'] != null){
          pedidos.add(f.data);
        }
      });
    });
    print(pedidos);
    return pedidos;
  }

  getDocumentID() async {
    final prefs = await SharedPreferences.getInstance();
    var id = prefs.get("documentID");
    var ref = prefs.get("ref");
    print("Referencia al documento:");
    print(ref);
    setState(() {
      documentId = id;
      docRef = ref;
    });
  }


  Future<void> initConnectivity() async {
    ConnectivityResult result;
    final prefs = await SharedPreferences.getInstance();


    var user = prefs.get("usuario");
    var id = prefs.get("documentID");

    try {
      result = await _connectivity.checkConnectivity();
      if (result == ConnectivityResult.mobile || result == ConnectivityResult.wifi) {
        List<String> nuevo = [];
        List lista = await getPedidos(user,id);

        if(lista.isNotEmpty) {
          lista.forEach((f)  =>  append(nuevo, f));
        }

        List remove = [];

        nuevo.forEach((l) {
          if (pedidos.contains(l)){
            remove.add(l);
          }
        });

        nuevo.removeWhere( (e) => remove.contains(e));
        print("Aqui");
        var x = nuevoValor().then((x1) async {
          if(x1 != null){
            print(x1);
            nuevo.add(x1);
            print("Si hay nuevo");
            await guardarFirebase(id);
          }
        });


        print("setState");
        setState(() {
          pedidos.addAll(nuevo);
        });

        guardarCache(nuevo);
      }

    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

  }

  guardarCache(List lista) async {

    var a = await widget.storage.readLine();

    var listaA = a.split(";");
    var str = '';
    lista.forEach((l){
      if(!listaA.contains(l)){
        str = str + l + ';';
      }
    }
    );

    await widget.storage.writeTransaction(a+str);

  }

  guardarFirebase( id) async {
    final prefs = await SharedPreferences.getInstance();
    var p = prefs.getString('producto');
    var m = await prefs.getString('medida');
    var c = await prefs.getInt('cantidad');
    var e = await prefs.getString('email');
    var op;
    var pr;
    var esta = await databaseReference
        .collection("usuarios")
        .getDocuments()
        .then((QuerySnapshot snapshot) {

      snapshot.documents.forEach((f) {
        if(f['ID'] == id){
          op = f.reference;
          f.documentID;
        }
        else if (f['email'] == e){
          pr = f.reference;
        }
      } );
    }).whenComplete((){
      databaseReference.runTransaction((transaction) async {
        await databaseReference.collection("prestamos")
            .add({
          'cantidad': c,
          'medida': m,
          'presta': pr,
          'producto': p,
          'recibe': op,
        });
      }).whenComplete(() async {
        print('Voy a borrar');
        await borrarSP();
      });
    });
  }

  append2(List lista, List lista2) async {
    List a = [];
    lista.forEach((f) async {
      print("volvi");
      var z = await f['presta'].get();
      var e = await z['email'];
      var h = f['presta'].documentID + " " + f['cantidad'].toString() + " " + f['medida'] +" "+ f['producto'] ;
      if (e != null) {
        var s = e + " " + f['cantidad'].toString() + " " + f['medida'] + " " +
            f['producto'];
        lista2.add(s);
        a.add(s);
        print("Se añadio");
        setState(() {
          pedidos.remove(h);
          pedidos.add(s);
        });
      }
    });
    return a;
  }

  append(List lista, p){
    var s = p['presta'].documentID + " " + p['cantidad'].toString() + " " + p['medida'] +" "+ p['producto'] ;
    lista.add(s);
  }



  cache() async{

    var a = await widget.storage.readLine();

    var lista = a.split(";");
    List<String> values = [];
    lista.forEach((l){
      if(!pedidos.contains(l)){
        values.add(l);
      }
    }
    );

    setState(() {
      pedidos.addAll(values);
    });

  }

  borrarSP() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('producto');
    prefs.remove('medida');
    prefs.remove('cantidad');
    prefs.remove('email');
  }

  nuevoValor() async {

    final prefs = await SharedPreferences.getInstance();

    // Try reading data from the counter key. If it doesn't exist, return 0.
    var e = await prefs.getString('email');
    if(e == null){
      print("Es null");
      return null;
    }
    print(e);
    var p = await prefs.getString('producto');
    var m = await prefs.getString('medida');
    var c = await prefs.getInt('cantidad');

    var tot = e + " " + c.toString()+ " " + m + " "+ p+";";

    print(tot);

    return tot;
  }

  Widget listaCache(BuildContext context){
    return ListView.separated(
      separatorBuilder: (context, index) => Divider(
        color: Colors.blueGrey,
      ),
      padding: new EdgeInsets.fromLTRB(0.0,20.0,0.0,20.0),
      itemCount: pedidos.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          height: 50,
          child: Center(child: Text('${pedidos[index]}',
            textScaleFactor: 1.0,
          )),
        );
      },
    );
  }

  Widget build2(BuildContext context) {


    return Scaffold(
      appBar: AppBar(
        backgroundColor: new Color.fromRGBO(249, 101, 116, 1.0),
        title: Text("Lo que debo",
            textScaleFactor: 1.0,
            style: TextStyle(fontSize: 25)),

      ),

      body: Container(
          child: new ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Colors.blueGrey,
            ),
            padding: new EdgeInsets.fromLTRB(0.0,20.0,0.0,20.0),
            itemCount: pedidos.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                height: 50,
                child: Center(child: Text('${pedidos[index]}',
                  textScaleFactor: 1.0,
                )),
              );
            },
          )
      ),
    );
  }

  void _refresh(int calif) {
    setState(() {
      _cal = calif;
    });
  }

  darCalificacion(DocumentSnapshot data){
    String str = "";
    if(data != null && data['calificacion'] != null){
      str = data['calificacion'].toString() + ' Estrellas';
    }
    else{
      str = 'No ha sido calificado';
    }
    return str;
  }

  Future<void> _alerta() async{
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Error de conexión',
            textScaleFactor: 1.0,
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Detectamos que no hay conexión a internet. Por el momento no se pueden calificar los productos',
                    textScaleFactor: 1.0,
                    style: TextStyle(fontSize: 20) ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Aceptar',
                textScaleFactor: 1.0,
                style: TextStyle(fontSize: 20, color: new Color.fromRGBO(249, 101, 116, 1.0),),),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot document){
    DocumentReference ref  = document['presta'];
    return FutureBuilder(
      future: ref.get(),
      builder: (context,snapshot){
        if(snapshot.connectionState == ConnectionState.done){
          if(snapshot.hasData && snapshot.data.exists) {
            return ListTile(
                subtitle: Text(
                  darCalificacion(document)
                ),
                title: Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        convertir(document, snapshot.data),
                        textScaleFactor: 1.0,
                      ),
                    ),
                    Container(
                        child: document['calificacion'] != null ? Container(height: 0.0,) :RaisedButton(
                          elevation: 5.0,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0)),
                          onPressed: () async {
                            if(document['calificacion'] != null){
                              return null;
                            }
                            ConnectivityResult result;
                            result = await _connectivity.checkConnectivity();
                            if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
                              if(document['calificacion'] != null){
                                return null;
                              }
                              else {
                                await _calif();
                                if(_cal > -1) {
                                  ref = Firestore.instance.document(
                                      'usuarios/' + docRef);
                                  Firestore.instance.runTransaction((
                                      transaction) async {
                                    DocumentSnapshot freshSnap =
                                    await transaction.get(document.reference);
                                    await transaction.update(
                                        freshSnap.reference, {
                                      'calificacion': _cal,
                                    });
                                  });
                                }

                                //Navigator.pushNamed(context, 'gracias');
                                //guardarAnalytics(
                                //    document.reference.documentID, documentId);
                              }
                            }
                            else{
                             return _alerta();
                            }

                          },
                          child: Text(
                            "Calificar",
                            textScaleFactor: 1.0,
                          ),
                          color: document['calificacion'] != null ? new Color.fromRGBO(150, 150, 150, 1.0) :new Color.fromRGBO(255, 213, 77, 1.0),
                        )
                    ),


                  ],

                )
            );
            return Container(
                height: 50,
                child: Center(
                  child: Text(
                    convertir(document, snapshot.data),
                    textScaleFactor: 1.0,
                  ),
                )
            );
          }
          else {
            return new Container();
          }
        }
        else {
          return new Container();
        }
      },
    );
  }


  String convertir(DocumentSnapshot doc, DocumentSnapshot data) {

    String str = "";
    if(data.exists) {
      if (data != null && data['nombre'] != null) {
        str = data['nombre'] + " " + doc['cantidad'].toString() + " " +
            doc['medida'] + " " + doc['producto'];
      }
      else if (data != null && data['email'] != null) {
        str = data['email'] + " " + doc['cantidad'].toString() + " " +
            doc['medida'] + " " + doc['producto'];
      }
    }

    return str;
  }

  List filtrar(snapshot) {
    List lista = [];
    snapshot.data.documents.forEach((f) {
      if (documentId != null && f['recibe'].documentID.toString().trim() == documentId && f['presta'] != null) {
        lista.add(f);
      }
    });
    print(lista.length);
    return lista;
  }

  Future<void> _calif() async{
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Calificar',
            textScaleFactor: 1.0,
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                new TextField(
                  maxLength: 1,
                  maxLines: 1,
                  keyboardType: TextInputType.number,
                  autofocus: false,
                  controller: _c,
                  decoration: new InputDecoration(
                      hintText: 'De 1 a 5',
                      icon: new Icon(
                        Icons.star,
                        color: Colors.grey,
                      )),

                ),
                Text("No puede ser un valor mayor a 5",
                style:TextStyle(fontSize: 15,),),
              ],
            ),
          ),
          actions: <Widget>[
              new FlatButton(
                child: Text('Aceptar',
                textScaleFactor: 1.0,
                style: TextStyle(fontSize: 20, color: new Color.fromRGBO(153, 190, 30, 1.0),),),
                onPressed: () {
                  if(int.parse(_c.text) <= 5) {
                    setState(() {
                      this._cal = int.parse(_c.text);
                    });
                    Navigator.of(context).pop();
                  }
                  else{
                    setState(() {
                      this._errorMessage = "No puede ser un valor mayor a 5";
                    });
                  }
              },
            ),
          ],
        );
      },
    );
  }

  Widget buildStream(BuildContext context){
    return StreamBuilder(
        stream: Firestore.instance.collection('prestamos').snapshots(),
        builder: (context, snapshot) {
          switch(snapshot.connectionState) {
            case ConnectionState.waiting:
              return listaCache(context);
            case ConnectionState.none:
              return listaCache(context);
            case ConnectionState.active:
            case ConnectionState.done:
              {
                if (!snapshot.hasData) {
                  return ListView(

                    children: <Widget>[
                      Container(
                          margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
                          child: Image.asset('assets/images/noHay.gif')
                      ),

                      Container(
                        height: 50,
                        margin: EdgeInsets.fromLTRB(35, 50, 35, 0),
                        child: Center(
                            child: Text('Aún no hay pedidos',
                                textAlign: TextAlign.center,
                                textScaleFactor: 1.0,
                                style: TextStyle(
                                  fontSize: 25,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ))),
                      ),
                    ],
                  );
                }
                var l = filtrar(snapshot);
                if (l.isEmpty) return ListView(
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
                        child: Image.asset('assets/images/noHay.gif')
                    ),

                    Container(
                      height: 50,
                      margin: EdgeInsets.fromLTRB(35, 50, 35, 0),
                      child: Center(
                          child: Text('No debes nada!',
                              textAlign: TextAlign.center,
                              textScaleFactor: 1.0,
                              style: TextStyle(
                                fontSize: 25,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              )
                          )
                      ),
                    ),
                  ],
                );
                return ListView.separated(
                  separatorBuilder: (context, index) =>
                      Divider(
                        color: Colors.blueGrey,
                      ),
                  itemCount: l.length,
                  itemBuilder: (context, index) =>
                      _buildListItem(context, l[index]),
                );
              }
            default: return new Container();
          }
        }
    );
  }

  Widget buildBody(BuildContext context) {

    return buildStream(context);
    //return ListView(
    //  padding: EdgeInsets.all(8),
    //  children: <Widget>[
    //    listaCache(context),
    //    buildStream(context)
    //    ],
    //);
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          backgroundColor: new Color.fromRGBO(249, 101, 116, 1.0),
          title: Text("Lo que debo",
              textScaleFactor: 1.0,
              style: TextStyle(fontSize: 25)),

        ),
        body: buildBody(context)
    );
  }

}
