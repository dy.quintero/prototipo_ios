import 'product.dart';

class ProductsRepository {
  static const _allProducts = <Product>[
    Product(
      category: Category.solidos,
      id: 0,
      isFeatured: true,
      name: 'Uvas',
      price: 120,
    ),
    Product(
      category: Category.solidos,
      id: 9,
      isFeatured: true,
      name: 'Papa',
      price: 58,
    ),
    Product(
      category: Category.solidos,
      id: 33,
      isFeatured: true,
      name: 'Sandia',
      price: 42,
    ),
    Product(
      category: Category.solidos,
      id: 33,
      isFeatured: true,
      name: 'Lechuga',
      price: 21,
    ),
    Product(
      category: Category.liquidos,
      id: 33,
      isFeatured: true,
      name: 'Leche',
      price: 21,
    ),
    Product(
      category: Category.solidos,
      id: 33,
      isFeatured: true,
      name: 'Mango',
      price: 21,
    ),
    Product(
      category: Category.liquidos,
      id: 33,
      isFeatured: true,
      name: 'Agua',
      price: 21,
    ),
    Product(
      category: Category.liquidos,
      id: 33,
      isFeatured: true,
      name: 'Cerveza',
      price: 21,
    ),
    Product(
      category: Category.solidos,
      id: 33,
      isFeatured: true,
      name: 'Cebolla',
      price: 21,
    ),
    Product(
      category: Category.solidos,
      id: 33,
      isFeatured: true,
      name: 'Platano',
      price: 21,
    ),
    Product(
      category: Category.solidos,
      id: 33,
      isFeatured: true,
      name: 'Manzana',
      price: 21,
    ),
    Product(
      category: Category.solidos,
      id: 33,
      isFeatured: true,
      name: 'Banano',
      price: 21,
    ),
    Product(
      category: Category.solidos,
      id: 33,
      isFeatured: true,
      name: 'Durazno',
      price: 21,
    ),
    Product(
      category: Category.solidos,
      id: 33,
      isFeatured: true,
      name: 'Naranja',
      price: 21,
    ),
    Product(
      category: Category.solidos,
      id: 33,
      isFeatured: true,
      name: 'Mandarina',
      price: 21,
    )
    // THIS IS A SAMPLE FILE. Get the full content at the link above.
  ];

  static List<Product> loadProducts(Category category) {
    if (category == Category.all) {
      return _allProducts;
    } else {
      return _allProducts.where((p) => p.category == category).toList();
    }
  }
}